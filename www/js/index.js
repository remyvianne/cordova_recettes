function get_recipe(recipeId) {
  app.request({
    url: "https://www.themealdb.com/api/json/v1/1/lookup.php?i=" + recipeId, //URL de L'api
    method: "GET", // Méthode 
    dataType: "json", // Important, sinon vous allez récupérer un string et non un objet
    beforeSend: function () {
      // Avant de récupérer mes datas, j'affiche un loader 
      //(important quand on fait un traitement pour montrer qu'il est en cours +  empêcher les impatients de cliquer partout pendant le process !)
      app.dialog.preloader();
    },
    success: function (res) {
      //Traitement des datas
      for (let index = 0; index < res.meals.length; index++) {
        let meal = res.meals[index];
        $$(".recipe__title").html(meal.strMeal);
        $$(".recipe__description").html(meal.strInstructions);
      }
      // Je ferme le loader
      app.dialog.close();
    },
  });
}

function initRecipe() {
  app.request({
    url: "https://www.themealdb.com/api/json/v1/1/list.php?c=list",//URL de L'api
    method: "GET", // Méthode 
    dataType: "json", // Important, sinon vous allez récupérer un string et non un objet
    beforeSend: function () {
      // Avant de récupérer mes datas, j'affiche un loader 
      //(important quand on fait un traitement pour montrer qu'il est en cours +  empêcher les impatients de cliquer partout pendant le process !)
      app.dialog.preloader();
    },
    success: function (res) {

      //Traitement des datas

      let meals = res.meals;
      $$(".page-content").empty();
      let nbMeal = 0;
      $$(".page-content").append(`
          <div class="block">`);
      meals.forEach((meal) => {
        
        if(nbMeal % 2 == 0){
          $$(".page-content .block").append(`
            <div class="row row${nbMeal}">
              <div class="col">
                <div class="card demo-card-header-pic" onClick="searchRecipe('${meal.strCategory}','c')">
                  <div style="background-image: url('./../img/${meal.strCategory.toLowerCase()}.png');" class="card-header align-items-flex-end"><h3 style="background: rgba(150,150,150, 0.3);">${meal.strCategory}</h3></div>
                </div>
              </div>`
        );
      }else{
       
          // console.log(".page-content .block .row"+(nbMeal-1)+"");
          $$(".page-content .block .row"+(nbMeal-1)+"").append(`
              <div class="col">
                <div class="card demo-card-header-pic"  onClick="searchRecipe('${meal.strCategory}','c')">
                  <div style="background-image: url('./../img/${meal.strCategory.toLowerCase()}.png');" class="card-header align-items-flex-end"><h3 style="background: rgba(150,150,150,0.3);">${meal.strCategory}</h3></div>
                </div>`
        );
      }
        nbMeal += 1;
      });
      $$(".page-content").append(`
          </div>`);
      // Je ferme le loader
      app.dialog.close();

    },
  });
}

async function get_category_recipes(query){
  const result = await app.request({
    url: "https://www.themealdb.com/api/json/v1/1/filter.php?c=" + query,//URL de L'api
    method: "GET", // Méthode 
    dataType: "json", // Important, sinon vous allez récupérer un string et non un objet
  });
  return result.data.meals
}

async function get_recipes(query) {
  const result = await app.request({
    url: "https://www.themealdb.com/api/json/v1/1/search.php?s=" + query,//URL de L'api
    method: "GET", // Méthode 
    dataType: "json", // Important, sinon vous allez récupérer un string et non un objet
  });
  return result.data.meals
}

async function searchRecipe(query, typeSearch) {
  $$(".page-content").empty();
  let meals;
  
  if(query){
    if(typeSearch === "c"){
      meals = await get_category_recipes(query);
    }else{
      meals = await get_recipes(query);
    }
  }
  if (!meals || meals.length == 0) {
    console.error("Meals null or empty");
  }else{
  meals.forEach((meal) => {
    $$(".page-content").append(`
      <div class="card">
        <div class="card-content card-content-padding">
        <a href="/recette/${meal.strMeal}">
          <div class="row display-flex align-items-center text-align-center">
            <div class="col-25">
              <img data-src="${meal.strMealThumb}" alt="image ${meal.strMeal.replace(/ /g, "_")}" width="50px" height="50px" class="lazy lazy-fade-in">
            </div >
          
            <div class="col-50">
              <p>${meal.strMeal}</p>
            </div>
            <div class="col-25">
                <i id="${meal.idMeal}_heart" class="icon f7-icons" onclick="addRecipeFavorite(this)">heart</i>
                <i id="${meal.idMeal}_heart" class="icon f7-icons" onclick="removeRecipeFavorite(this)">heart_fill</i>
            </div>
          </div>
        </a>
        </div>
      </div>`
      );
      app.lazy.create('.homepage');
    });
  } 
}
  
  // function createRowsInit() {
  
  //   let divBlock = document.createElement('div');
  //   divBlock.classList.add("block");
  
  //   let divRow = document.createElement('div');
  //   divRow.classList.add("row");
  //   divRow.classList.add("no-gap");
  
  
  //   let divCol1 = document.createElement('div');
  //   divCol1.classList.add("col-15");
  //   divCol1.classList.add("display-flex");
  
  //   let img = document.createElement('img');
  //   img.classList.add("lazy");
  //   img.classList.add("meal_img");
  //   divCol1.appendChild(img);
  
  //   let divCol2 = document.createElement('div');
  //   divCol2.classList.add("col-50");
  
  //   let meal_title = document.createElement("h2");
  //   meal_title.classList.add("meal_title");
  //   divCol2.appendChild(meal_title)
  
  //   let divCol3 = document.createElement('div');
  //   divCol3.classList.add("col-35");
  //   divRow.appendChild(divCol1);
  //   divRow.appendChild(divCol2);
  //   divRow.appendChild(divCol3);
  //   divBlock.appendChild(divRow);
  //   document.querySelector(".page-content").appendChild(divBlock);
  // }