function recette(nomRecette) {
  app.request({
    url: "https://www.themealdb.com/api/json/v1/1/search.php?s=" + nomRecette,//URL de L'api
    method: "GET", // Méthode 
    dataType: "json", // Important, sinon vous allez récupérer un string et non un objet
    beforeSend: function () {
      // Avant de récupérer mes datas, j'affiche un loader 
      //(important quand on fait un traitement pour montrer qu'il est en cours +  empêcher les impatients de cliquer partout pendant le process !)
      app.dialog.preloader();
    },
    success: function (res) {
      //Traitement des datas
      let meal = res.meals[0];
      $$(".meal_title").html(meal.strMeal);
      $$(".meal_img").attr("src", meal.strMealThumb);
      $$(".strInstructions").html(meal.strInstructions);
      for (let i = 1; i < 20; i++) {
        if (eval("meal.strIngredient" + i)) {
          console.log("path")
          $$(".divIngredient").append("<div class='swiper-slide'>" + "<p class='strMeasure" + i + " strIngredient" + i + "'></p>" + "</div>");
          $$(".strMeasure" + i).html(eval("meal.strMeasure" + i));
          $$(".strIngredient" + i).append(" " + eval("meal.strIngredient" + i));
        }
      }
      $$(".strInstructions").attr(meal.strInstructions);
      // Je ferme le loader
      app.dialog.close();
    },
  });
} 