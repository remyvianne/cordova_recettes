
var routes = [
  {
    path: '/',
    url: './index.html',
  }, {
    path: '/home/',
    url: './pages/home.html',
    on: {
      pageAfterIn: function (event, page) {
        // do something after page gets into the view
        let router = this;
        let app = router.app;
        let searchBar = app.searchbar.create({
          customSearch: true,
          el: '.searchbar',
          on: {
            enable: function () {
            },
            search: async function (e) {
              await searchRecipe(e.query);
            },
            disable: function () {
              initRecipe();
            }
          }
        });
      },
      pageInit: function (event, page) {
        // do something when page initialized
        initRecipe();
      },
    },
  },
  {
    path: '/recette/:nomRecette',
    componentUrl: './pages/recette.html',
    on: {
      pageInit: function (event, page) {
        const { nomRecette } = page.route.params;
        console.log(nomRecette);
        recette(nomRecette);
      },
    },
  },
  {
    path: '/favorites/',
    componentUrl: './pages/favorites.html',
    on: {
      pageInit: function (event, page) {
      },
    },
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
